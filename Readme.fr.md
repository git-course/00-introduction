# 📚 Git Learning - Introduction

This Readme is available in: [English](./Readme.md), [Français](./Readme.fr.md).

## 🚥 Point de Départ

Bienvenue élève et félicitations pour ton admission à la prestigieuse **École de Git**.

À l'**École de Git**, tu pourras apprendre comment utiliser efficacement notre merveilleux `git`. Mais avant toute chose, il y a certaines règles que tu dois absolument respecter pour pouvoir tirer le meilleur parti de nos différents cours rigoureux :

1. Tu promets de ne **jamais** utiliser d'interface graphique pour résoudre tes problèmes. Tu apprendras en utilisant ton **terminal** et ton **interpréteur shell** préférés. Ton instructeur utilisera `Alacritty` et `zsh`, mais tu es libre d'utiliser ce qui te convient le mieux.
2. Tu suivras tous les cours sans exception, dans l'ordre que tu souhaites. Ne pas en suivre un peut te sembler être une bonne idée, mais ton instructeur a été victime de nombreuses incompréhensions qu'il avait avec `git`, et va tenter de les éclaircir. Nous recommandons de suivre les cours dans l'ordre spécifié.
3. Une pratique régulière, tu auras.
4. Tu créditeras l'**École de Git** grâce à un lien dès que tu partageras ses ressources.

Une fois que tu auras clairement répété chaque règle individuellement et que tu les auras toutes acceptées, nous pourrons commencer.

## 📔 Prérequis

Avant d'officiellement devenir élève à l'**École de Git**, ton équipement doit correspondre à nos besoins.

- Tu dois posséder un terminal capable d'exécuter `git` et des commandes basiques de `bash`.
- Tes outils sont à jour.
- Tu pourras préférer l'utilisation de Linux car l'ensemble des cours ont été écrits et testés sous ArchLinux. Ils devraient fonctionner avec n'importe quel OS supportant `git` et `bash` mais l'**École de Git** n'offre aucune garantie.

## 💻 Mise en place de l'environnement

Chaque élève espérant un jour posséder la **Maîtrise de Git** se doit de configurer correctement son environnement pour travailler dans les meilleures conditions. L'école te guidera pour ce premier exemple pratique.

### 🕵 Spécifie ton identité

La partie la plus importante d'un dépôt `git` est la capacité de savoir qui a fait quoi, et quand. Nou savons trop souvent vu des élèves utiliser leur identifiant professionnel... mais n'oublie pas que personne ne sait qui est *KEORP3472*. Pas que *Robert Jr.* soit plus connu, mais au moins nous avons un nom.

Puisque nous avons su te convaincre, nous avons besoin que tu vérifies ton identité `git` avant d'entrer dans l'**École de Git**. Tu peux le faire en entrant dans ton terminal `git config --global --get user.name`. Si la réponse ne te satisfait pas ou qu'elle ne correspond pas à nos attentes, merci de la changer.

```bash
git config --global user.name "Robert Jr."
```

Les règles sont les mêmes pour ton adresse mail. Elle doit être identique à celle définie sur ton compte **Gitlab** (ou tout autre hébergeur que tu utilises) pour que le lien avec ton compte puisse être effectué. Ce n'est *pas* obligatoire en soi, mais nous te le **recommandons fortement**.

Vérifie ton adresse grâce à `git config --global --get user.email` et modifie-la si nécessaire.

```bash
git config --global user.email "robert.jr@gitlab.com"
```

### 📝 Choisis ton éditeur préféré

Pour être un ou une **Élève Git** digne de ce nom, il faut t'équiper des meilleurs outils. Peu importe si tu penses qu'il s'agit de `nvim`, `nano`, `atom` ou n'importe quel autre éditeur, tu souhaites sûrement l'équiper aussi vite que possible (*après tout c'est un butin légendaire*).

Mais avant que tu ne sautes sur la configuration, nous voulons te mettre en garde. L'**École de Git** interdit formellement l'utilisation d'interfaces graphiques pour `git` et tu devras donc entrer toutes tes commandes dans un terminal. L'éditeur que tu vas définir lors de ta prochaine commande est extrêmement important parce qu'il va souvent s'ouvrir pour te demander d'entrer des messages. Par conséquent, l'utilisation d'un éditeur graphique peut te faire perdre beaucoup de temps, pour l'ouvrir et le fermer à chaque fois.

En gardant cette information en tête, nous te laissons faire ton choix. Tu peux récupérer l'éditeur actuellement configuré avec `git config --global --get core.editor`. Si nécessaire, édite-le.

```bash
git config --global core.editor nano
```

Bienvenue à l'**École de Git** ! Nous espérons que tu apprécieras ce temps parmi nous et que tu obtiendras rapidement ton diplôme ! Bonne chance et n'oublie pas de t'amuser et de boire suffisamment.

Tu peux commencer ton apprentissage avec le cours [01 - Init and Clone](https://gitlab.com/git-course/01-init-and-clone).

## 🌟 Bonus : Dictionnaire

*Psst ! Tu es toujours là ?*

Pour t'aider dans ta quête du savoir, j'ai rédigé une liste de vocabulaire ultra-méga-compliqué que tu dois connaître. Si tu ne comprends pas tout dès maintenant, ne panique pas ! La liste ne disparaîtra pas et tu pourras y revenir quand tu le jugeras nécessaire.

*Ces définitions sont simplifiées et peuvent ne pas refléter exactement la réalité.*

**Repository *(ou repo, dépôt)* :** Sur ton ordinateur, il correspond au dossier `.git` qui contient des informations utiles pour l'exécution de `git`. Il te permet d'avoir une copie du *repo* distant à une version particulière (un *commit*).
À distance, il s'agit plus ou moins de l'*historique* complet du projet. Il contient tous les changements et est positionné sur une version particulière (un *commit* spécifique dans une certaine *branche*).

**Commit :** Il s'agit de la liste des changements effectués sur le code depuis le dernier *commit*. Imagine-le comme une snapshot de ton code. Ton *historique* complet est une succession de *commits* qui spécifient ce qui a changé à travers le temps.

**Branch :** Considères que ton *historique* est un arbre. Quand tu crées uniquement des *commits*, ils dépendent tous du précédent, en ligne. Les *branches* the permettent de diverger de ce chemin de base (la *branche main*) et de travailler sur deux chemins différents en même temps. Ils ont le même tronc (un *commit* de base), mais les *branches* elles-même sont différentes (des modifications différentes dans plusieurs *commits*). Elles peuvent se rejoindre (*merge*) plus tard dans le processus de développement.

**Merge :** Processus de réunion de deux *branches* différentes dans l'une d'entre elles. Il y a plusieurs manières d'effectuer un *merge* que nous verrons dans un tutoriel plus loin.

**Push :** L'action d'envoyer tous les *commits* que tu as créés localement dans le *repo* distant.

**Pull :** Manière simple d'effectuer la combinaison de `git fetch` et `git merge` automatiquement. Nous verrons ces commandes plus en détail plus tard, mais pour l'instant il suffit de comprendre que le *fetch* permet de télécharger les changements faits sur le *repo* distant et le *merge* de les incorporer aux fichiers locaux.

Ces commandes peuvent paraître sombres actuellement, mais l'**École de Git** est là pour te guider. Tu comprendras ces concepts en un rien de temps !