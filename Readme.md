# 📚 Git Learning - Introduction

This Readme is available in: [English](./Readme.md), [Français](./Readme.fr.md).

## 🚥 Starting Point

Welcome student, and congratulations for joining the prestigious **Git School**.

Here at **Git School**, you will learn how to efficiently use our savior `git`. But before you start your first lesson, there are few rules that you must follow in order to learn the most from our rigorous training:

1. You shall not use any GUI to resolve your problems. You will learn using your preferred **terminal** and **shell interpretor**. Your instructor will use `Alacritty` and `zsh`, but feel free to use what suits you best.
2. You will follow all the different activities in the order you want. Skipping one may seem best at first, but your instructor have also had some misconceptions about `git` before, and try to address them here. We however recommend following the specified order.
3. Practice regularly, you will.
4. Credit **Git School** with a link whenever you share its resources, you will.

Now that you've clearly repeated each and every rule and accepted them, we may begin.

## 📔 Requirements

Before officially becoming a **Git School** student, you must meet the following criteria.

- You have a working terminal, able to execute `git` and basic `bash` commands.
- Your tools are up to date.
- You may prefer working with Linux as all projects have been tested under ArchLinux. They may work under any OS with `git` and `bash` but this is not a guarantee.

## 💻 Setting up your environment

Any student hoping to once become a **Git Master** shall set up their environment to work in the best conditions. You will be guided in this first practical example.

### 🕵 Specifying your Identity

The most important thing in a `git` repository is to be able to know who did what, and when. We've seen too many students use their school or work identifier... but remember that no one knows who *KEORP3472* is. Not that we know who *Robert Jr.* is, but at least we have a name.

So before you enter **Git School**, we need you to check your current `git` identity. You can do so by typing `git config --global --get user.name` in your terminal. If the configuration does not help identify you in any way, please change it.

```bash
git config --global user.name "Robert Jr."
```

The same applies for your email. It must be the same as the one in **Gitlab** (or any host that you might use) in order for it to make the link with your account. This is *not* mandatory but **highly recommended**.

Check your email in `git config --global --get user.email` and modify it if needed.

```bash
git config --global user.email "robert.jr@gitlab.com"
```

### 📝 Choosing your preferred editor

A true **Git Student** must be equipped with the best tools. Whether you think it is `nvim`, `nano`, `atom` or whatever suits you best, you surely want to equip it as soon as possible (*after all it's a legendary loot*).

But before you jump right in, you want to know a few things. Since **Git School** prohibits the use of GUI for using `git`, you'll enter all your commands in a terminal. The editor you will configure in the next command is extremely important as it will often pop up to ask you for input. Therefore, having a GUI editor may lead to a long time lost waiting for it to open.

With that in mind, let's jump right in. As usual, you may check your currently defined editor with `git config --global --get core.editor`. If necessary, edit it.

```bash
git config --global core.editor nano
```

Welcome to **Git School**, we hope you'll enjoy your stay here and that you will graduate soon! Good luck, and remember to have fun and stay hydrated.

You can start learning with the lesson [01 - Init and Clone](https://gitlab.com/git-course/01-init-and-clone).

## 🌟 Bonus: Dictionary

*Psst! Still here, student?*

To help you through your journey, I've written a list of ultra-hard vocabulary that you need to know. If you do not understand everything right now, do not panic! The list won't disappear, and you can come back at any time to look back at a definition.

*Please note that these descriptions are simplified and may not exactly reflect the truth.*

**Repository *(or repo):*** It corresponds locally to the `.git` directory which contains useful information for `git`. It allows us to have a copy of our remote repo for a given version (a *commit*).
Remotely, it is more or less the complete *history* of your project. It contains all your changes and is set to a specific version (a specific *commit* in a specific *branch*).

**Commit:** It is a list of changes to the code since the last *commit*. Think of it as a snapshot of your code. Your complete *history* is a succession of different *commits* specifying what changed throughout time.

**Branch:** Let's think of our *history* as a tree. When you only create *commits*, they are all depending on the previous one, in line. *Branches* allow you to diverge from the *main* path (the *main branch*) and to work on two different paths at the same time. They have the same trunk (the base *commit*), but the *branches* themselves are different (different modifications in different *commits*). They may *merge* back later in the development process.

**Merge:** The process of reuniting the changes made in two differents *branches* in one of them. There are different ways to *merge* that we will cover later.

**Push:** The act of sending all the *commits* you've created locally to the remote *repo*.

**Pull:** It is an easy way to do `git fetch` followed by a `git merge` automatically. We'll cover that in depth later too, but the *fetch* downloads all changes made on the remote *repo* and the *merge* part will apply them to your local files. 

This may seem obscure right now, but **Git School** has you covered, you'll understand all these concepts in no time!